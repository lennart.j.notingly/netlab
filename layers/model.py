import numpy as np
from neurons.model import neuron
from neurons.functionalneuron import functionalneuron


class layer():
    def __init__(self, neurons, neuron_type=neuron):
        self.neurons = np.array(neurons, dtype=neuron_type)

    def __mul__(self, other):
        return self.neurons * other

    def __len__(self):
        return self.neurons.size


create_layer = lambda size, previous_layer,neuron_creator = None, layer_type = layer, neuron_type = functionalneuron: \
    layer_type(
        neurons=[(
            neuron_creator(
                previous_layer=previous_layer,
            ) if neuron_creator is not None  # if neuron creater is none use neuron type as creator
            else neuron_type(
                previous_layer=previous_layer,
            )
        )
            for i in range(size)
        ],
        neuron_type=neuron_type
    )
# neuron creator can be a function that creates a neuron with the type neuron_type
# it can be the neuron type too
