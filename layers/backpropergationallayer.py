import layers.assembledlayer
from neurons.activationfunctionneurons import sigmoid_differentiation, sigmoid
import neurons.backpropergationalneurons
import layers.functionallayer
from tools.neuronmultiplyer import multiply_neuron


class backpropergationallayer(layers.functionallayer.functionallayer):
    def reset_error(self):
        for neuron in self.neurons:
            neuron.reset_error()

    def connect_next_neuron(self, next_neuron):
        for neuron in self.neurons:
            neuron.connect_next_neuron(next_neuron)

    def calc_error(self, optimal_output_values):
        for i in range(len(self.neurons)):
            self.neurons[i].calc_error(optimal_output_values[i])

    def adjust_connections(self, learning_parameter):
        for neuron in self.neurons:
            neuron.adjust_connections(learning_parameter)


class backpropergationaloutputlayer(backpropergationallayer):
    pass


class assembledbackpropergationlayer(layers.assembledlayer.assembledlayer):
    def reset_error(self):
        for layer in self.layers:
            layer.reset_error()

    def connect_next_layer(self, next_layer):
        for layer in self.layers:
            layer.connect_next_layer(next_layer)

    def calc_error(self, optimal_output_values):
        for i in range(len(self.layers)):
            self.layers[i].calc_error(optimal_output_values[i])

    def adjust_connections(self, learning_parameter):
        for layer in self.layers:
            layer.adjust_connections(learning_parameter)


create_backpropergationallayer = lambda size, previous_layer, next_layer, neuron_type=neurons.backpropergationalneurons.deltaoutputneuron, activation_function=sigmoid, differencial_activation_function=sigmoid_differentiation:\
    backpropergationallayer(
        multiply_neuron(
            neuron_type(previous_layer, activation_function=activation_function, differencial_activation_function=differencial_activation_function),
            size,
            previous_layer
        ),
        neuron_type=neuron_type
    )
