import numpy as np
import layers.model
import tools.errors


class inputlayer():
    values = None

    def __init__(self, size):
        self.size = size
        self.values = np.zeros(shape=(size), dtype=np.float)

    def init_values(self, values):
        if len(values) != self.size:
            raise tools.errors.DiffrentArraySizeError
        self.values = np.array(values)

    def __mul__(self, other):
        return self.values * other

    def __len__(self):
        return len(self.values)

    def calc(self):  # to prevent from tools from things that try to call this function
        pass

    def rand(self, change_factor):  # to prevent from tools from things that try to call this function
        pass
