import layers.model
import neurons.functionalneuron
import neurons.activationfunctionneurons


class functionallayer(layers.model.layer):
    def get_values(self):
        return [this_neuron.value for this_neuron in self.neurons]

    def rand(self, randomize_factor):
        for neuron in self.neurons:
            neuron.rand(randomize_factor)

    def calc(self):
        for neuron in self.neurons:
            neuron.calc()

    def connect_new_previous_layer(self, previous_layer):
        for neuron in self.neurons:
            neuron.connect_new_previous_layer(previous_layer)


create_functionallayer = lambda size, previous_layer, neuron_creator=neurons.activationfunctionneurons.BasicSigmoidNeuronCreator, neuron_type = neurons.functionalneuron.functionalneuron: layers.model.create_layer(
    size=size,
    layer_type=functionallayer,
    previous_layer=previous_layer,
    neuron_type=neuron_type,
    neuron_creator=neuron_creator
)
