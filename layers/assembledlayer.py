import numpy as np


class assembledlayer():  # is a hidden layer that handels multible vertical layers
    def __init__(self, layers=None):
        if layers is None:
            layers = []
        self.layers = layers

    def add_layer(self, layer):
        self.layers += layer

    def calc(self):
        for layer in self.layers:
            layer.calc()

    def rand(self, change_factor):
        for layer in self.layers:
            layer.rand(change_factor)

    def __len__(self):
        total_lenght = 0
        for layer in self.layers:
            total_lenght += len(layer)
        return total_lenght

    def __mul__(self, neuron_connections):
        sum = np.array([], dtype=np.float)
        connection_index = 0
        for layer in self.layers:
            sum = np.append(sum, layer * neuron_connections[connection_index:connection_index + len(layer)])
            connection_index += len(layer)
        return sum

    def get_values(self):
        values = []
        for layer in self.layers:
            values += layer.get_values()
        return values
