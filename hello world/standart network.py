import neurons.functionalneuron
import neurons.basicadditionalparamneuron
import neurons.activationfunctionneurons
import layers.inputlayer
import layers.functionallayer
import layers.model
import layers.assembledlayer
import networks.evolutionalalgorythmcustomizednetworks
import tools.connect
import tools.evolutionalalgorythm

input_layer = layers.inputlayer.inputlayer(5)
hidden_layer1 = layers.model.create_layer(
    size=70,
    previous_layer=input_layer,
    layer_type=layers.functionallayer.functionallayer,
    neuron_creator=neurons.activationfunctionneurons.BasicArctanNeuronCreator
)
hidden_layer2 = layers.model.create_layer(
    size=120,
    previous_layer=hidden_layer1,
    layer_type=layers.functionallayer.functionallayer,
    neuron_creator=neurons.basicadditionalparamneuron.BasicArctanNeuronCreator,
    neuron_type=neurons.basicadditionalparamneuron.basicadditionalparamneuron
)

hidden_layer3 = layers.assembledlayer.assembledlayer(layers=[
    layers.functionallayer.create_functionallayer(5, hidden_layer2),
    layers.functionallayer.create_functionallayer(3, hidden_layer2, neuron_creator=neurons.functionalneuron.functionalneuron)
])

output_layer = layers.model.create_layer(
    size=4,
    previous_layer=hidden_layer3,
    layer_type=layers.functionallayer.functionallayer
)
tools.connect.connect(layers.assembledlayer.assembledlayer(layers=[output_layer,hidden_layer1]), hidden_layer2)
network = networks.evolutionalalgorythmcustomizednetworks.threadedevolutionalalgorythmnetwork(layers=[input_layer, hidden_layer1, hidden_layer2,hidden_layer3, output_layer])


network.set_input([1, 0, 0, -1, 2])
network.rand(1)
network.calc()
print(network.get_output())


AI = tools.evolutionalalgorythm.EvolutionAlgorythm(network, threaded=False)

import time
t1 = time.time()
for i in range(10):
    print(i)
    AI.clone_and_change(count_of_clones=50, change_factor=0.02)
    AI.reset_error()

    AI.set_input([1, 2, -1, 1, 0])
    AI.calc()
    AI.set_input([1, 0, 0, -1, 2])
    AI.calc()
    AI.determine_error([1, -1, 0, 2])

    AI.set_input([3, -2, 1, 1, 1])
    AI.calc()
    AI.set_input([1, 0, 0, -1, 2])
    AI.calc()
    AI.determine_error([1, 1, -1, 3])

    AI.determine_new_main_network()
t2 = time.time()
print(t1-t2)
onlearned_network = AI.main_network


onlearned_network.set_input([1, 2, -1, 1, 0])
onlearned_network.calc()
onlearned_network.set_input([1, 0, 0, -1, 2])
onlearned_network.calc()
print(onlearned_network.get_output())
import tools.save

tools.save.save(onlearned_network,filename="myNetwork")
