from neurons import backpropergationalneurons
from layers import inputlayer
from tools.neuronmultiplyer import multiply_neuron
from neurons import activationfunctionneurons
import layers.backpropergationallayer
k = inputlayer.inputlayer(3)
h = layers.backpropergationallayer.backpropergationaloutputlayer(
    neurons=
    multiply_neuron(
        backpropergationalneurons.deltaoutputneuron(k, activation_function=activationfunctionneurons.sigmoid, differencial_activation_function=activationfunctionneurons.sigmoid_differentiation),
        10,
        k
    ),
    neuron_type=backpropergationalneurons.deltaoutputneuron
)
i = layers.backpropergationallayer.backpropergationallayer(neurons=[
    backpropergationalneurons.deltaoutputneuron(h, activation_function=activationfunctionneurons.sigmoid, differencial_activation_function=activationfunctionneurons.sigmoid_differentiation),
    backpropergationalneurons.deltaoutputneuron(h),
    backpropergationalneurons.deltaoutputneuron(h, activation_function=activationfunctionneurons.sigmoid,differencial_activation_function=activationfunctionneurons.sigmoid_differentiation),
    backpropergationalneurons.deltaoutputneuron(h, activation_function=activationfunctionneurons.sigmoid,differencial_activation_function=activationfunctionneurons.sigmoid_differentiation)
], neuron_type=backpropergationalneurons.deltaoutputneuron)

i.rand(1)
h.rand(1)

for j in range(10000):
    k.init_values([-0.5, -0.1, 0.3])
    h.calc()
    i.calc()
    i.calc_error([1,0.3,0.7,0.1])

    i.adjust_connections(0.2)
    i.reset_error()
    print(i.get_values())
