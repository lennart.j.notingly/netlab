
# import time
# t1 = time.time()
# network = networks.evolutionalalgorythmcustomizednetworks.threadedevolutionalalgorythmnetwork(layers=[input_layer, hidden_layer1, hidden_layer2,hidden_layer3, output_layer])
#
# # optimal_out = [-1,2,1,0]
# # network.rand(1)
# # network.set_input([1,5,2,1,1])
# # network.calc()
# # print(network.get_output())
# #
# # network.set_input([1,5,2,1,1])
# # network.calc()
# # print(network.get_output())
#
#
#
# AI = tools.evolutionalalgorythm.EvolutionAlgorythm(network, threaded=False)
#
# for i in range(100):
#     print(i)
#     AI.clone_and_change(count_of_clones=50, change_factor=0.15)
#     AI.reset_error()
#
#     AI.set_input([1, 2, -1, 1, 0])
#     AI.calc()
#     AI.set_input([1, 0, 0, -1, 2])
#     AI.calc()
#     AI.determine_error([1, -1, 0, 2])
#
#     AI.set_input([3, -2, 1, 1, 1])
#     AI.calc()
#     AI.set_input([1, 0, 0, -1, 2])
#     AI.calc()
#     AI.determine_error([1, 1, -1, 3])
#
#     AI.determine_new_main_network()
# t2 = time.time()
# onlearned_network = AI.main_network
#
#
#
# onlearned_network.set_input([3, -2, 1, 1, 1])
# onlearned_network.calc()
# onlearned_network.set_input([1, 0, 0, -1, 2])
# onlearned_network.calc()
# print(onlearned_network.get_output())
#
# print(t2 - t1)

import numpy as np
from neurons import backpropergationalneurons
from layers import inputlayer
i = inputlayer.inputlayer(3)
o = backpropergationalneurons.deltaoutputneuron(i)
o.connections = np.array([0.6, 0.3, -0.2])
for k in range(2000):
    i.init_values([10,-5,7])
    o.calc()
    o.calc_error(2)
    print(o.value)
    print(o.connections)
    o.adjust_connections(0.005)

    i.init_values([3, 2, -3])
    o.calc()
    o.calc_error(-1)
    print(o.value)
    print(o.connections)
    o.adjust_connections(0.005)

    i.init_values([-3, 0, 1])
    o.calc()
    o.calc_error(0)
    print(o.value)
    print(o.connections)
    o.adjust_connections(0.005)

    i.init_values([1, -1, 4])
    o.calc()
    o.calc_error(1)
    print(o.value)
    print(o.connections)
    o.adjust_connections(0.005)
