from copy import deepcopy as clone
import layers.model
import layers.functionallayer
import layers.inputlayer
import networks.functionalnetwork
import neurons.activationfunctionneurons
import neurons.basicadditionalparamneuron
import layers.assembledlayer
import neurons.functionalneuron
import tools.output_compaire

input_layer = layers.inputlayer.inputlayer(5)
hidden_layer1 = layers.model.create_layer(
    size=7,
    previous_layer=input_layer,
    layer_type=layers.functionallayer.functionallayer,
    neuron_creator=neurons.activationfunctionneurons.BasicArctanNeuronCreator
)
hidden_layer2 = layers.model.create_layer(
    size=12,
    previous_layer=hidden_layer1,
    layer_type=layers.functionallayer.functionallayer,
    neuron_creator=neurons.basicadditionalparamneuron.BasicArctanNeuronCreator,
    neuron_type=neurons.basicadditionalparamneuron.basicadditionalparamneuron
)

hidden_layer3 = layers.assembledlayer.assembledlayer(layers=[
    layers.functionallayer.create_functionallayer(5, hidden_layer2),
    layers.functionallayer.create_functionallayer(3, hidden_layer2, neuron_creator=neurons.functionalneuron.functionalneuron)
])

output_layer = layers.model.create_layer(
    size=4,
    previous_layer=hidden_layer3,
    layer_type=layers.functionallayer.functionallayer
)
network1 = networks.functionalnetwork.functionalnetwork(layers=[input_layer, hidden_layer1, hidden_layer2,hidden_layer3, output_layer])

network1.rand(1)
network2 = clone(network1)
optimal_out = [-1,2,1,0]
for i in range(200):
    network2.rand(0.1)
    network1.set_input([1, 4, 2, 4, 0])
    network2.set_input([1, 4, 2, 4, 0])
    network1.calc()
    network2.calc()
    if tools.output_compaire.compaire_output(network1.get_output(),optimal_out) < tools.output_compaire.compaire_output(network2.get_output(),optimal_out):
        network2 = clone(network1)
    else:
        network1 = clone(network2)
    print(network1.get_output())

