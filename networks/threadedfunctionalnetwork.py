import threading
import networks.functionalnetwork


class threadedfunctionalnetwork(networks.functionalnetwork.functionalnetwork, threading.Thread):
    def __init__(self, layers):
        self.layers = layers
        threading.Thread.__init__(self)

    def run(self) -> None:
        self.calc()
