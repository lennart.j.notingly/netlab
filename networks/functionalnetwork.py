import networks.model


class functionalnetwork(networks.model.network):
    def calc(self):
        for layer in self.layers:
            layer.calc()

    def rand(self, change_factor):
        for layer in self.layers:
            layer.rand(change_factor)

    def get_output(self):
        return self.layers[-1].get_values()

    def set_input(self, input_values):
        self.layers[0].init_values(input_values)
