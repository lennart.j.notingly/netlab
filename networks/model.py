class network:
    def __init__(self,layers):
        self.layers = layers

    def iterateLayers(self,function):
        for layer in self.layers:
            function(layer)
