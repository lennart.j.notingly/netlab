from copy import deepcopy as clone
import tools.output_compaire
import networks.functionalnetwork
import networks.threadedfunctionalnetwork


class evolutionalalgorythmnetwork(networks.functionalnetwork.functionalnetwork):
    error = 0
    revard_function = tools.output_compaire.compaire_output
    def clone(self):
        return clone(self)

    def calc_error(self, compare_data):  # compare data will be compared with output
        self.error += self.revard_function(self.get_output(), compare_data)


class threadedevolutionalalgorythmnetwork(networks.threadedfunctionalnetwork.threadedfunctionalnetwork):
    error = 0
    revard_function = tools.output_compaire.compaire_output

    def clone(self):
        cloned_self = threadedevolutionalalgorythmnetwork(clone(self.layers))
        cloned_self.error = clone(self.error)
        cloned_self.revard_function = self.revard_function
        return cloned_self

    def calc_error(self, compare_data):  # compare data will be compared with output
        self.error += self.revard_function(self.get_output(), compare_data)
