def connect(first_layer, second_layer):  # connects first and second layer
    second_layer.connect_new_previous_layer(first_layer)
    try:
        first_layer.connect_next_layer(second_layer)
    except AttributeError:
        pass
