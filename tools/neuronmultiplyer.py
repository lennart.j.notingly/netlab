from copy import deepcopy as copy


def multiply_object(object,multiplyer:int):
    all_objects = []
    for i in range(multiplyer):
        all_objects += [copy(object)]
    return all_objects


def multiply_neuron(neuron, multiplyer, previous_layer, next_layer=None):
    all_neurons = multiply_object(neuron, multiplyer)
    for neuron in all_neurons:
        neuron.previous_layer = previous_layer
        if next_layer is not None:
            neuron.next_layer = next_layer
    return all_neurons
