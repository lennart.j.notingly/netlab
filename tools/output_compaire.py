import numpy as np


def compaire_output(self,real_output, optimal_output):
    return abs(np.array(real_output,dtype = np.float) - np.array(optimal_output, dtype = np.float)).sum()