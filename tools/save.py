import dill
import os
from zipfile import ZipFile
import zipfile
from compile import compileClasses


def save(network, filename):
    try:
        file = open(filename + ".unn", mode="xb")
    except FileExistsError:
        file = open(filename + ".unn", mode="wb")
    file.write(dill.dumps(network))
    with ZipFile(filename+'.nn', 'w', compression=zipfile.ZIP_DEFLATED) as myzip:
        myzip.write(filename + ".unn")
    os.remove(filename + ".unn")


def load(filename):
    with ZipFile(filename + ".nn", mode="r") as myzip:
        myzip.extractall()
    with open(filename + ".unn", mode="rb") as file:
        content = dill.loads(file.read())
        file.close()
    os.remove(filename + ".unn")
    return content


import networks.model
import networks.evolutionalalgorythmcustomizednetworks
import networks.threadedfunctionalnetwork
import networks.functionalnetwork

import neurons.model
import neurons.backpropergationalneurons
import neurons.activationfunctionneurons
import neurons.basicadditionalparamneuron
import neurons.functionalneuron

import layers.model
import layers.assembledlayer
import layers.inputlayer
import layers.functionallayer
def comprime_to_simpleNetwork(your_network, type_compiles = None):
    if type_compiles is None:
        type_compiles = [
            (networks.model.network, networks.functionalnetwork.functionalnetwork(layers=[0])),
            (networks.evolutionalalgorythmcustomizednetworks.evolutionalalgorythmnetwork, networks.functionalnetwork.functionalnetwork(layers=[0])),
            (networks.evolutionalalgorythmcustomizednetworks.threadedevolutionalalgorythmnetwork, networks.functionalnetwork.functionalnetwork(layers=[0])),
            (networks.threadedfunctionalnetwork.threadedfunctionalnetwork, networks.functionalnetwork.functionalnetwork(layers=[0])),

            (neurons.model.neuron, neurons.functionalneuron.functionalneuron(previous_layer=[0])),
            (neurons.backpropergationalneurons.hiddenbackpropergationneuron, neurons.functionalneuron.functionalneuron(previous_layer=[0])),
            (neurons.backpropergationalneurons.deltaoutputneuron,neurons.functionalneuron.functionalneuron(previous_layer=[0])),
            #(neurons.basicadditionalparamneuron.basicadditionalparamneuron, neurons.basicadditionalparamneuron.basicadditionalparamneuron(previous_layer=[0])),

            (layers.model, layers.functionallayer.functionallayer(neurons=[])),
        ]
    return compileClasses.compileObject(your_network,typeCompilations=type_compiles)
