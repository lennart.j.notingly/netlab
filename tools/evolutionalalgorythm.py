from copy import deepcopy as clone


class EvolutionAlgorythm:
    networks = []

    def __init__(self, network, threaded=False):
        self.main_network = network
        self.threading_option = threaded

    def clone_and_change(self, count_of_clones, change_factor):  # clone and change main network
        networks = [self.main_network.clone() for i in range(count_of_clones)]

        for network in networks:
            network.rand(change_factor=change_factor)

        networks += [self.main_network.clone()]

        self.networks = networks

    def reset_error(self):  # set error to 0 in every network
        for network in self.networks:
            network.error = 0

    def set_input(self, input_values):  # post input values to every network
        for network in self.networks:
            network.set_input(input_values)

    def calc(self):  # calculate output
        if self.threading_option:
            for i in range(len(self.networks)):
                self.networks[i] = self.networks[i].clone()
            for network in self.networks:
                network.start()
            for network in self.networks:
                network.join()
        else:
            for network in self.networks:
                network.calc()

    def calculate(self, input_data):  # post input and calc
        self.set_input(input_data)
        self.calc()

    def determine_error(self, compare_data):  # calculate and add additionaly error
        for network in self.networks:
            network.calc_error(compare_data)

    def calc_error(self, input_data, compare_data):  # post input, calculate and add error od this round
        self.calculate(input_data)
        self.determine_error(compare_data)

    def determine_new_main_network(self):  # compare what is the best network and set main network to this
        best_network = self.networks[0]
        for network in self.networks:
            if network.error < best_network.error:
                best_network = network
        self.main_network = best_network.clone()
        self.main_network.error = 0
