from neurons.activationfunctionneurons import *
import neurons.functionalneuron
import numpy as np


class basicadditionalparamneuron(neurons.functionalneuron.functionalneuron):
    def init(self):
        self.connections = np.zeros(shape=(len(self.previous_layer)), dtype=np.float)
        self.shifting_factor = np.float(0)
        self.smoothing_factor = np.float(1)

    def rand(self, change_factor):
        self.connections += 2 * (np.random.rand(len(self.previous_layer)) - 0.5) * change_factor
        self.shifting_factor += np.float(0.1) * ((np.random.random_sample() - 0.5) * change_factor)
        self.smoothing_factor += np.float(0.1) * ((np.random.random_sample() - 0.5) * change_factor)

    def calc(self):
        self.value = self.activation_function(
            self.smoothing_factor * (self.previous_layer * self.connections).sum() + self.shifting_factor
        )

BasicAdditionalParamLinearNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=linear
    )

BasicAdditionalParamBinaryStepNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=binary_step
    )

BasicAdditionalParamSigmoidNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=sigmoid
    )

BasicAdditionalParamTangensHyperbolicusNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=tangens_hyperbolicus
    )

BasicAdditionalParamReLuNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=ReLu
    )

BasicAdditionalParamSoftPlusNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=SoftPlus
    )

BasicAdditionalParamELuNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=ELu
    )

BasicAdditionalParamArctanNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=arctan
    )

BasicAdditionalParamSinusNeuronCreator = lambda previous_layer: \
    basicadditionalparamneuron(
        previous_layer,
        activation_function=sinus
    )
