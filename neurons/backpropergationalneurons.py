import numpy as np
import neurons.functionalneuron


class hiddenbackpropergationneuron(neurons.functionalneuron.functionalneuron):
    error = 0
    index_in_layer = None
    next_layer = None

    def connect_next_layer(self, next_layer, index_in_this_layer):
        self.index_in_layer = index_in_this_layer
        self.next_layer = next_layer

    def reset_error(self):
        self.error = np.float(0)

    def calc_error(self):
        self.error += np.array([
            neuron_in_next_layer.error * neuron_in_next_layer.connections[self.index_in_layer]
            for neuron_in_next_layer in self.next_layer
        ], dtype=np.float()).sum()

    def adjust_connections(self, learning_parameter):
        self.connections += self.previous_layer.get_values() * self.error * learning_parameter
        self.reset_error()


class deltaoutputneuron(neurons.functionalneuron.functionalneuron):
    error = 0

    def __init__(self, previous_layer, activation_function=lambda x: x, differencial_activation_function=lambda x: 1):
        self.differencial_activation_function = differencial_activation_function
        self.previous_layer = previous_layer
        self.value = np.float(0)
        self.activation_function = activation_function
        self.init()

    def reset_error(self):
        self.error = np.float(0)

    def connect_next_layer(self):
        pass

    def calc_error(self, optimal_output_value):
        try:
            self.error += np.float(optimal_output_value - self.value) * self.differencial_activation_function(
                np.array(self.previous_layer.get_values(), dtype=np.float).sum()
            )
        except AttributeError:
            self.error += np.float(optimal_output_value - self.value) * self.differencial_activation_function(
                np.array(self.previous_layer.values, dtype=np.float).sum()
            )

    def adjust_connections(self, learning_parameter):
        try:
            self.connections += np.array(self.previous_layer.get_values(),dtype=np.float) * self.error * learning_parameter
        except AttributeError:
            self.connections += self.previous_layer.values * self.error * learning_parameter
        self.reset_error()
