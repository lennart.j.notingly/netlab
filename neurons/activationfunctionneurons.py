import numpy as np
import neurons.functionalneuron


linear = lambda x: x
binary_step = lambda x: np.float(0) if x < 0 else np.float(1)
sigmoid = lambda x: 1/(1 + np.exp(-x))
tangens_hyperbolicus = lambda x: np.tanh(x)
ReLu = lambda x: np.float(0) if x < 0 else x
SoftPlus = lambda x: np.ln(1 + np.exp(x))
ELu = lambda x, a = np.float(1.67326): a * (np.exp(x) - 1) if x < 0 else x# Exponential linear unit
arctan = lambda x: np.arctan(x)
sinus = lambda x: np.sin(x)

linear_differentiation = lambda x=0: 1
binary_step_differentiation = lambda x=0: np.float(0.05) # real differantionion is just 0 but for better results with backpropergation
sigmoid_differentiation = lambda x: sigmoid(x)*(1 - sigmoid(x))
tangens_hyperbolicus_differentiation = lambda x: 1/np.cosh(x)**2
ReLu_differentiation = lambda x: np.float(0.05) if x < 0 else np.float(1)
SoftPlus_differentiation = lambda x: np.exp(x)/(np.exp(x) + 1)
ELu_differentiation = lambda x, a = np.float(1.67326): a * np.exp(x) if x < 0 else 1
arctan_differentiation = lambda x: 1/(x**2 + 1)
sinus_differentiation = lambda x: np.cos(x)

BasicLinearNeuronCreator = lambda previous_layer: \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=linear,
    )

BasicBinaryStepNeuronCreator = lambda previous_layer: \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=binary_step,
    )

BasicSigmoidNeuronCreator = lambda previous_layer: \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=sigmoid,
    )

BasicTangensHyperbolicusNeuronCreator = lambda previous_layer: \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=tangens_hyperbolicus,
    )

BasicReLuNeuronCreator = lambda previous_layer, : \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=ReLu,
    )

BasicSoftPlusNeuronCreator = lambda previous_layer: \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=SoftPlus,
    )

BasicELuNeuronCreator = lambda previous_layer, : \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=ELu,
    )

BasicArctanNeuronCreator = lambda previous_layer: \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=arctan,
    )

BasicSinusNeuronCreator = lambda previous_layer: \
    neurons.functionalneuron.functionalneuron(
        previous_layer,
        activation_function=sinus,
    )