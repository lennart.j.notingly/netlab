import numpy as np


class neuron:
    def __init__(self, previous_layer, activation_function=lambda x: x):
        self.previous_layer = previous_layer
        self.value = np.float(0)
        self.activation_function = activation_function
        self.init()

    def init(self):
        pass

    def clac(self):
        self.value = self.activation_function(self.previous_layer.sum())

    def __mul__(self, other):
        return self.value * other
