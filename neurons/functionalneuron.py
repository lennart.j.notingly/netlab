import neurons.model
import numpy as np


class functionalneuron(neurons.model.neuron):
    def init(self):
        self.connections = np.zeros(shape=(len(self.previous_layer)), dtype=np.float)

    def calc(self):
        self.value = self.activation_function((self.previous_layer * self.connections).sum())

    def rand(self, change_factor):
        self.connections += 2*(np.random.rand(len(self.previous_layer)) - 0.5) * change_factor

    def connect_new_previous_layer(self, previous_layer):
        self.previous_layer = previous_layer
        self.connections = np.zeros(shape=(len(self.previous_layer)), dtype=np.float)
